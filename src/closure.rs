
struct BaleOfStraw {
    size: usize
}

fn closure_examples() {
    // Recap: Anatomy of a closure
    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~
    //
    let rummage = | element | {  // 👈  input parameters MAY omit type annotation
                                                //     if type can be inferred. The input parameter
                                                //     is also referred to as the "captured value".
        println!("found: {}", element);
                                                // 👈  no return statement: closures *can* return values,
                                                //     but this one doesn't
    };                                          // 👈  {}s are only needed for multi-line closures

    let haystack = vec!["hay", "hay", "hay", "needle", "hay", "hay"];

    // REFERENCING CLOSURES
    //=====================
    // *By default*, closures capture variables by reference. When given a choice, this is the
    // behaviour inferred by the compiler.

    haystack.iter().for_each(rummage);
    // note that we can directly borrow the contents of `haystack` after using the `rummage`
    // closure, because it only borrowed the elements (and is now done)
    println!("top of the haystack: {}", haystack[0]);

    // MUTATING CLOSURES
    //==================
    // Closures can mutate the variables they are capturing

    // 👀  Closures can be used as function arguments.

    // ✅ TODO: remove all the hay from `haystack` by checking whether `key` is a needle
    let mut haystack_clone = haystack.clone();
    haystack_clone.retain(|key| *key == "needle");
    println!("look, I found the needle: {:?}", haystack_clone);

    // 👀  a common use case for closures is to transform collections
    //     using e.g. `map()` and `filter()`.

    // ✅ TODO: use `map()` to increase the bale size with every "hay" in the haystack
    let mut bale = BaleOfStraw{ size: 0 };

    // let new_haystack: Vec<_> = haystack
    //     .into_iter()
    //     .filter(|element | *element == "hay")
    //     .map(| thing | { bale.size += 1; thing} )
    //     .collect();

    // ✅ TODO: try uncommenting this line. What happens when you re-compile and why?
    // println!("haystack: {:?}", haystack );

    // ✅  Bonus Task: re-implement the creation of `new_haystack` using `filter_map()`
    //     https://doc.rust-lang.org/std/iter/trait.Iterator.html#method.filter_map

    let new_haystack: Vec<_> = haystack
        .into_iter()
        .filter_map(|foo|
                    if foo == "hay" { bale.size += 1; Some(foo) }
                    else { None })
        .collect();

    println!("bale size: {}", bale.size);
    println!("new haystack: {:?}", new_haystack);
}

#[cfg(test)]
mod tests {
    use super::closure_examples;

    #[test]
    fn run_closure_examples() {
        closure_examples();
    }
}
