


fn strings_examples() {
    // &'static str
    let _this = "Hallo";
    // String
    let that: String = String::from("Hallo");
    // &str
    let _other = that.as_str();
    let _foo = "thing";

    println!("The final result is: {}\n", that + " world");

    let part_one = String::from("Hello ");
    let part_two = String::from("there ");
    let whole = part_one + &part_two + "world!";
    println!("{}", whole);

    let words = "things people say";
    let split_words: Vec<_> = words.split(" ").collect();
    println!("we have: {:?}", split_words);

    for foo in words.split(" ") {
        println!("{}", foo);
    }
}

#[cfg(test)]
mod tests {
    use super::strings_examples;

    #[test]
    fn run_strings_examples() {
        strings_examples();
    }
}
