
#[derive(PartialEq, Debug)]
enum FarmAnimal {
    Worm,
    Cow,
    Bull,
    Chicken { num_eggs: usize },
    Dog { name: String },
    Sheep,
}

impl std::fmt::Display for FarmAnimal {
        fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
           match *self {
            FarmAnimal::Dog {name: _} => write!(f, "Dog"),
            FarmAnimal::Chicken {num_eggs: _} => write!(f, "Chicken"),
            _ => write!(f, "{:?}", self)
        }
    }
}

fn what_does_the_animal_say(animal: &FarmAnimal) {

    let noise = match animal {
        FarmAnimal::Cow | FarmAnimal::Bull => "moo".to_string(),
        FarmAnimal::Chicken{ num_eggs: _ } => "cluck, cluck!".to_string(),
        FarmAnimal::Dog{ name } if name == "Lassie" =>
            format!("What's that Lassie, someones stuck down the well?"),
        FarmAnimal::Dog{ name } => format!("woof, woof! I am {}!", name),
        &FarmAnimal::Sheep => "baa".to_string(),
        _ => "-- (silence)".to_string(),
    };

    println!("{} says: {}", animal, noise);
}

fn match_examples() {
    what_does_the_animal_say(
        &FarmAnimal::Dog {
            name: "Lassie".to_string()
    });
    what_does_the_animal_say(
        &FarmAnimal::Dog {
            name: "Buster".to_string()
    });
    what_does_the_animal_say(&FarmAnimal::Cow);
    what_does_the_animal_say(&FarmAnimal::Bull);
    what_does_the_animal_say(&FarmAnimal::Chicken{num_eggs: 3});
    what_does_the_animal_say(&FarmAnimal::Worm);
    what_does_the_animal_say(&FarmAnimal::Sheep);
}

#[cfg(test)]
mod tests {
    use super::match_examples;

    #[test]
    fn run_match_examples() {
        match_examples();
    }
}
