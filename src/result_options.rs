
use std::fs::File;
use std::io::prelude::*;
use std::io::BufReader;
use url::Url;

fn parse_url(line: String) -> Option<Url>
{
    match Url::parse(&line) {
        Ok(x) => Some(x),
        Err(_) => None,
    }
}

fn result_options_examples() -> Result<(), std::io::Error> {

    let f = File::open("src/data/content.txt")?;
    let reader = BufReader::new(f);
    let mut length = 0;
    let mut not_urls = 0;

    for line in reader.lines() {
        if let Ok(line) = line {
            length += line.len();
            if let Some(url) = parse_url(line) {
                println!("found URL: {}", url);
            } else {
                not_urls += 1;
            }
        }
    }

    println!("Read {} bytes, {} were not URLs", length, not_urls);

    Ok(())
}

#[cfg(test)]
mod tests {
    use super::{result_options_examples, parse_url};
    use matches::assert_matches;

    #[test]
    fn correct_url() {
        assert!(parse_url(String::from("https://example.com")).is_some())
    }

    #[test]
    fn no_url() {
        assert!(parse_url(String::from("abcdf")).is_none())
    }

    #[test]
    fn run_result_options_examples() {
        assert_matches!(result_options_examples(), Ok(()));
    }
}
